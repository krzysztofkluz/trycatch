﻿using BusinessLayer;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace TestProject
{
    static class TestBusinessLayer
    {
        public static void TestSaving()
        {
            XmlReader reader = XmlReader.Create(@"C:\Users\krzysztof.kluz\Documents\Visual Studio 2013\Projects\Trycatch\RestfulWS\App_Data\Articles.xml");
            List<Article> articles = Article.LoadArticles(reader).ToList<Article>();

            using (SqlConnection conn = new SqlConnection("Data Source=(LocalDB)\\v11.0;AttachDbFilename=\"C:\\Users\\krzysztof.kluz\\Documents\\Visual Studio 2013\\Projects\\Trycatch\\RestfulWS\\App_Data\\WebShop.mdf\";Integrated Security=True"))
            {
                SqlCommand sqlCmd = new SqlCommand("sp_ClearTables", conn);
                conn.Open();
                sqlCmd.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCmd.ExecuteNonQuery();
                conn.Close();
            }

            DictionaryShoppingCart sc = new DictionaryShoppingCart();
            foreach (Article a in articles)
            {
                sc.AddArticle(a);
            }
            string custEmail = "email@email.com";
            Customer c = new Customer("Mr", "first", "last", "address", "houseNum", "zipcode", "city", custEmail);
            sc.Save(c);

            WebShopDataContext wsdc = new WebShopDataContext();
            CustomerRecord cr =  wsdc.CustomerRecords.SingleOrDefault(cust => cust.EmailAddress == custEmail);
            if (cr!=null) Console.Out.WriteLine("First customer created successfully");
            else Console.Out.WriteLine("First customer is NOT created successfully");
            OrderRecord or = wsdc.OrderRecords.SingleOrDefault(ord => ord.CustomerId == cr.Id);
            if (or!=null) Console.Out.WriteLine("First order created successfully");
            else Console.Out.WriteLine("First order is NOT created successfully");
            var cart = from scItems in wsdc.ShoppingCartRecords
                       where scItems.OrderId == or.Id
                       select scItems;
            if(cart.Count() == articles.Count()) Console.Out.WriteLine("First order items created successfully");
            else Console.Out.WriteLine("First order items are NOT created successfully");

           

            sc.ClearArticles();
            sc.AddArticle(articles[0]);
            sc.AddArticle(articles[10]);
            c.EmailAddress = c.EmailAddress + "1";
            sc.Save(c);

            cr = wsdc.CustomerRecords.SingleOrDefault(cust => cust.EmailAddress == c.EmailAddress);
            if (cr != null) Console.Out.WriteLine("Second customer created successfully");
            else Console.Out.WriteLine("Second customer is NOT created successfully");

             //or = wsdc.OrderRecords.SingleOrDefault(ord => ord.CustomerId == cr.Id);
            or = (from o in wsdc.OrderRecords
                 where o.CustomerId == cr.Id
                 orderby o.Id descending
                 select o).First();
             if (or != null) Console.Out.WriteLine("Second order created successfully");
             else Console.Out.WriteLine("Second order is NOT created successfully");
             cart = from scItems in wsdc.ShoppingCartRecords
                       where scItems.OrderId == or.Id
                       select scItems;
             if (cart.Count() == sc.CountArticles()) Console.Out.WriteLine("Second order items created successfully");
             else Console.Out.WriteLine("Second order items are NOT created successfully");
        }

        public static void TestReadingArticlesXml()
        {
            XmlReader reader = XmlReader.Create(@"C:\Users\krzysztof.kluz\Documents\Visual Studio 2013\Projects\Trycatch\RestfulWS\App_Data\Articles.xml");
            IEnumerable<Article> articles = Article.LoadArticles(reader);
            reader.Close();
        }
    }
}
