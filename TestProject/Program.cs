﻿using BusinessLayer;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestProject
{
    class Program
    {
        static void Main(string[] args)
        {
            TestBusinessLayer.TestSaving();
            TestBusinessLayer.TestReadingArticlesXml();
            Console.WriteLine("Tests completed");
            Console.ReadKey();
        }


    }
}
