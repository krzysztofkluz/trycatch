﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestProject
{
    public class TestException : Exception
    {
       public TestException() { }
       public TestException(string message) : base(message) { }
    }
}
