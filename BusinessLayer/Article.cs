﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace BusinessLayer
{
    /// <summary>
    /// Represents a single article with its properties
    /// </summary>
    [Serializable]
    public class Article
    {
        #region private fields
        private int id;
        private string name;
        private decimal netPrice;
        private int vatRate;
        private string description;
        #endregion
        #region public properties
        /// <summary>
        /// Unique identifier representing the article
        /// </summary>
        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        /// <summary>
        /// Name of the article
        /// </summary>
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        /// <summary>
        /// Net price of the article
        /// </summary>
        public decimal NetPrice
        {
            get { return netPrice; }
            set { netPrice = value; }
        }
        /// <summary>
        /// Vat rate of the article
        /// </summary>
        public int VatRate
        {
            get { return vatRate; }
            set { vatRate = value; }
        }
        /// <summary>
        /// Price of the article including Vat
        /// </summary>
        public decimal GrossPrice
        {
            get { return netPrice + (netPrice * vatRate / 100); }
        }
        /// <summary>
        /// Amount of Vat in the Gross Price
        /// </summary>
        public decimal VatValue
        {
            get { return GrossPrice - NetPrice; }
        }
        /// <summary>
        /// Description of the article
        /// </summary>
        public string Description
        {
            get { return description; }
            set { description = value; }
        }
        #endregion

        //empty default constructor is needed for javascript deserialization
        public Article(){ }

        public Article(int id, string name, decimal netPrice, int vatRate, string desc)
        {
            this.id = id;
            this.name = name;
            this.netPrice = netPrice;
            this.vatRate = vatRate;
            this.description = desc;
        }

        /// <summary>
        /// Static Method returns all articles stored in the xml file
        /// </summary>
        /// <param name="reader">An XMLReader object acessing the data source</param>
        /// <returns>Array of article objects</returns>
        public static IEnumerable<Article> LoadArticles(XmlReader reader)
        {
            List<Article> articles = new List<Article>();

            /*Simple XML to LINQ usage*/
            XElement xelement = XElement.Load(reader);
            IEnumerable<XElement> xarticles = xelement.Elements();
            foreach (var a in xarticles)
            {
                /* Valdation of attributes */
                int id;
                if (a.Attribute("id") == null || !Int32.TryParse(a.Attribute("id").Value, out id)) 
                {throw new Exception("Mandatory attibute 'id' not found"); }
                
                string name;
                if (a.Attribute("name") != null) { name = Convert.ToString(a.Attribute("name").Value); }
                else { throw new Exception("Mandatory attibute 'name' not found"); }

                decimal netPrice;
                if (a.Attribute("netPrice") == null || !Decimal.TryParse(a.Attribute("netPrice").Value, out netPrice))
                { throw new Exception("Mandatory attibute 'netPrice' not found"); }

                //If vatRate is not specified assume 0% rate
                int vatRate;
                if (a.Attribute("vatRate") == null || !Int32.TryParse(a.Attribute("vatRate").Value, out vatRate)) 
                { vatRate = 0; }

                string desc;
                if (a.Attribute("desc") != null) { desc = Convert.ToString(a.Attribute("desc").Value); }
                else { desc = string.Empty; }

                Article newArticle = new Article(id, name, netPrice, vatRate, desc);
                articles.Add(newArticle);
            }
            return articles.AsEnumerable<Article>();

        }
    }
}
