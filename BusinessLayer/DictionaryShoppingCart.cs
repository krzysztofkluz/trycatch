﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class DictionaryShoppingCart : IShoppingCart
    {
        #region private fields
        private Dictionary<Article,int> articlesList;
        #endregion
        #region public properties

        /// <summary>
        /// Sum of gross value of all items in the cart
        /// </summary>
        public decimal TotalShoppingCartValueInclVat
        {
            get
            {
                decimal result = 0;
                foreach (Article a in articlesList.Keys)
                {
                    result += a.GrossPrice * articlesList[a];
                }
                return result;
            }
        }
        /// <summary>
        /// Sum of net value of all items in the cart
        /// </summary>
        public decimal TotalShoppingCartValueExclVat
        {
            get
            {
                decimal result = 0;
                foreach (Article a in articlesList.Keys)
                {
                    result += a.NetPrice * articlesList[a];
                }
                return result;
            }
        }
        /// <summary>
        /// Sum of Vat value of all items in the cart
        /// </summary>
        public decimal TotalShoppingCartVatValue
        {
            get
            {
                decimal result = 0;
                foreach (Article a in articlesList.Keys)
                {
                    result += a.VatValue * articlesList[a];
                }
                return result;
            }
        }

        #endregion

        public DictionaryShoppingCart()
        {
            articlesList =  new Dictionary<Article, int>();
        }

        /// <summary>
        /// Method adds an article to the shopping cart
        /// </summary>
        /// <param name="art">Article object to be added to the shopping cart</param>
        /// <returns>Amount of articles in the shopping cart after the new element is inserted</returns>
        public int AddArticle(Article art){
            if (articlesList.ContainsKey(art))
            {
                articlesList[art]++;
            }
            else
            { 
                articlesList.Add(art, 1);
            
            }
            int sum =0;
            foreach(int i in articlesList.Values){
                sum += i;
            }
            return sum;
        }


        /// <summary>
        /// Method removes an article from the shopping cart
        /// </summary>
        /// <param name="art">Article object to be removed from the shopping cart</param>
        /// <returns>Amount of articles in the shopping cart after the element was removed</returns>
        public int RemoveArticle(Article art)
        {
            if (articlesList.ContainsKey(art))
            {
                if (articlesList[art] > 1)
                {
                    articlesList[art]--;
                }
                else
                {
                    articlesList.Remove(art);
                }
            }
            else
            {
                throw new Exception("There is no such article in the cart");
            }

            int sum =0;
            foreach(int i in articlesList.Values){
                sum += i;
            }
            return sum;
        }
        
        /// <summary>
        /// Removes all items from the shopping cart
        /// </summary>
        public void ClearArticles()
        {
            articlesList.Clear();
        }
        /// <summary>
        /// Counts items in the shopping cart
        /// </summary>
        /// <returns>amount of articles in the shopping cart</returns>
        public int CountArticles()
        {
            int sum = 0;
            foreach (int i in articlesList.Values)
            {
                sum += i;
            }
            return sum;
        }
        /// <summary>
        /// Stores a shopping cart with a given customer
        /// </summary>
        /// <param name="cust">Customer to be linked with shopping cart</param>
        public void Save(Customer cust )
        {
            WebShopDataContext wsdc = new WebShopDataContext();
            CustomerRecord customerInDb = wsdc.CustomerRecords.SingleOrDefault(c => c.EmailAddress == cust.EmailAddress);
            if (customerInDb == null)
            {
                customerInDb = new CustomerRecord();
                customerInDb.Address = cust.Address;
                customerInDb.City = cust.City;
                customerInDb.EmailAddress = cust.EmailAddress;
                customerInDb.FirstName = cust.FirstName;
                customerInDb.HouseNumber = cust.HouseNumber;
                customerInDb.LastName = cust.LastName;
                customerInDb.Title = cust.Title;
                customerInDb.ZipCode = cust.ZipCode;
                wsdc.CustomerRecords.InsertOnSubmit(customerInDb);
            }
            OrderRecord or = new OrderRecord();
            
            wsdc.OrderRecords.InsertOnSubmit(or);
            customerInDb.OrderRecords.Add(or);
            foreach (Article a in articlesList.Keys)
            {
                ShoppingCartRecord scr = new ShoppingCartRecord();
                scr.Amount = articlesList[a];
                scr.ArticleId = a.Id;
                or.ShoppingCartRecords.Add(scr);
                
                
            }
            
            wsdc.SubmitChanges();
        }

        /// <summary>
        /// Gets a collection of articles in the shopping cart
        /// </summary>
        /// <returns>IEnumerable collection of articles</returns>
        public IEnumerable<Article> GetArticles()
        {
            List<Article> a = new List<Article>();
            foreach (Article art in articlesList.Keys)
            {
                for (int i = 0; i < articlesList[art]; i++)
                {
                    a.Add(art);
                }
            }
            return a.AsEnumerable<Article>();
        }

    }
}
