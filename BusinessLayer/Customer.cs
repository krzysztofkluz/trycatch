﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    /// <summary>
    /// A class representing a customer doing online shopping
    /// </summary>
    public class Customer
    {
        #region private fields
                private int id;
                private string title;
                private string firstName;
                private string lastName;
                private string address;
                private string houseNumber;
                private string zipCode;
                private string city;
                private string emailAddress;
        #endregion
        #region public properties
        /// <summary>
        /// Id of the customer
        /// </summary>
        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        /// <summary>
        /// First name of the customer
        /// </summary>
        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }
        /// <summary>
        /// Title of the customer
        /// </summary>
        public string Title
        {
            get { return title; }
            set { title = value; }
        }
        /// <summary>
        /// Last name of the customer
        /// </summary>
        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }
        /// <summary>
        /// Address of the customer
        /// </summary>
        public string Address
        {
            get { return address; }
            set { address = value; }
        }
        /// <summary>
        /// House number of the customer
        /// </summary>
        public string HouseNumber
        {
            get { return houseNumber; }
            set { houseNumber = value; }
        }
        /// <summary>
        /// Zip code of the customer
        /// </summary>
        public string ZipCode
        {
            get { return zipCode; }
            set { zipCode = value; }
        }
        /// <summary>
        /// City of the customer
        /// </summary>
        public string City
        {
            get { return city; }
            set { city = value; }
        }
        /// <summary>
        /// Email address of the customer
        /// </summary>
        public string EmailAddress
        {
            get { return emailAddress; }
            set { emailAddress = value; }
        }
        #endregion

        public Customer(string title, string firstName, string lastName, string address, string houseNumber, string zipCode, string city, string emailAddress)
        {
            this.Title = title;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Address = address;
            this.HouseNumber = houseNumber;
            this.ZipCode = zipCode;
            this.City = city;
            this.EmailAddress = emailAddress;
        }
    }
}
