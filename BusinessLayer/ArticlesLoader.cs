﻿using BusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

namespace BusinessLayer
{
    /// <summary>
    /// Static class gathering methods returning a collection of Article objects
    /// </summary>
    public static class ArticlesLoader
    {
        /// <summary>
        /// Method to read content of a xml under a given file path
        /// </summary>
        /// <param name="filePath">Location of a xml file</param>
        /// <returns>IEnumerable collection of articles</returns>
        public static IEnumerable<Article> Load(string filePath)
        {
            IEnumerable<Article> a;
            XmlReader reader = XmlReader.Create(filePath);
            a = Article.LoadArticles(reader);
            reader.Close();
            reader.Dispose();
            return a;
        }
    }
}