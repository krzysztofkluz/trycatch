﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public interface IShoppingCart
    {
        /// <summary>
        /// Sum of gross value of all items in the cart
        /// </summary>
         decimal TotalShoppingCartValueInclVat { get;  }
         /// <summary>
         /// Sum of net value of all items in the cart
         /// </summary>
         decimal TotalShoppingCartValueExclVat { get;  }
         /// <summary>
         /// Sum of Vat value of all items in the cart
         /// </summary>
         decimal TotalShoppingCartVatValue { get;  }
         /// <summary>
         /// Method adds an article to the shopping cart
         /// </summary>
         /// <param name="art">Article object to be added to the shopping cart</param>
         /// <returns>Amount of articles in the shopping cart after the new element is inserted</returns>
         int AddArticle(Article art);
         /// <summary>
         /// Method removes an article from the shopping cart
         /// </summary>
         /// <param name="art">Article object to be removed from the shopping cart</param>
         /// <returns>Amount of articles in the shopping cart after the element was removed</returns>
         int RemoveArticle(Article art);
         /// <summary>
         /// Removes all items from the shopping cart
         /// </summary>
         void ClearArticles();
         /// <summary>
         /// Counts items in the shopping cart
         /// </summary>
         /// <returns>amount of articles in the shopping cart</returns>
         int CountArticles();
         /// <summary>
         /// Stores a shopping cart with a given customer
         /// </summary>
         /// <param name="cust">Customer to be linked with shopping cart</param>
         void Save(Customer cust);
         /// <summary>
         /// Gets a collection of articles in the shopping cart
         /// </summary>
         /// <returns>IEnumerable collection of articles</returns>
         IEnumerable<Article> GetArticles();
    }
}
