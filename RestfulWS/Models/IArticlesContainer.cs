﻿using BusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestfulWS.Models
{
    public interface IArticlesContainer
    {
        IEnumerable<Article> GetArticles();

    }
}
