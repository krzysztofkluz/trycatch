﻿using BusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace RestfulWS.Models
{
    public class XmlArticlesContainer : IArticlesContainer
    {
        /// <summary>
        /// Gets a collection of Articles read from xml file
        /// </summary>
        /// <returns>IEnumerable collection of articles</returns>
        public IEnumerable<Article> GetArticles()
        {

                return ArticlesLoader.Load(WebConfigurationManager.AppSettings["XmlFileLocation"]);

        }

    }
}