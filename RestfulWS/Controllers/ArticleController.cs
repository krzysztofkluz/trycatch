﻿using BusinessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Mvc;
using System.Xml;
using RestfulWS.Models;

namespace RestfulWS.Controllers
{

    public class ArticleController : ApiController
    {
        private IArticlesContainer container;
        /// <summary>
        /// Default constructor. An example of dependency injection design pattern usage
        /// </summary>
        /// <param name="container"></param>
        public ArticleController(IArticlesContainer container)
        {
            this.container = container;
        }


        // GET api/article/
        /// <summary>
        /// Gets a colletion of articles read from the data container
        /// </summary>
        /// <returns>IEnumerable collection of Articles</returns>
        public IEnumerable<Article> GetArticles()
        {
                try
                {
                    return container.GetArticles();
                }
                catch
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError));
                }
        }



        

    }
}
