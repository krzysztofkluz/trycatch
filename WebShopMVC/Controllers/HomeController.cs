﻿using BusinessLayer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace WebShopMVC.Controllers
{
    public class HomeController : ArticlesLoaderController
    {
        public ActionResult Index()
        {
            IEnumerable<Article> arts = LoadArticles();
            //ViewBag.PageSize & ViewBag.ArticlesAmount are used to calculate if there is next page in the result set
            ViewBag.PageSize = WebConfigurationManager.AppSettings["PageSize"];
            ViewBag.ArticlesAmount = arts.Count<Article>();

            //Shopping cart content is kept in the session
            IShoppingCart cart = (IShoppingCart)Session["cart"];
            ViewBag.CartCount =  (cart==null)?0:cart.CountArticles();
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Contact author.";

            return View();
        }

        /// <summary>
        /// Gets a partial collection of articles to be displayed on a page
        /// </summary>
        /// <param name="page">Page number to be returned</param>
        /// <returns>Json object containing collection of articles to be dispayed on a page</returns>
        public JsonResult GetArticles(int page)
        {
            IEnumerable<Article> arts = LoadArticles();
            int pageSize = Convert.ToInt32(WebConfigurationManager.AppSettings["PageSize"]);
            arts = arts.Skip(pageSize * page).Take(pageSize);
            return Json(arts, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets single article details
        /// </summary>
        /// <param name="id">Id of the article to be returned</param>
        /// <returns>Json object with details of the article </returns>
        public JsonResult GetArticle(int id)
        {
            IEnumerable<Article> arts = LoadArticles();
            Article art = arts.SingleOrDefault(a=>a.Id == id);
            return Json(art, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Adds articles to cart
        /// </summary>
        /// <param name="id">Id of the article to be put in cart</param>
        /// <returns>Amount of articles in shopping cart</returns>
        public int PutToCart(int id)
        {
            IShoppingCart cart = null;
            if (Session["cart"] == null) {
                cart = new DictionaryShoppingCart(); 
            }
            else
            {
                cart = (IShoppingCart)Session["cart"];
            }
            Article art = LoadArticles().SingleOrDefault(a=>a.Id == id);
            int counter = cart.AddArticle(art);
            Session["cart"] = cart;
            return counter;
        }


    }
}