﻿using BusinessLayer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace WebShopMVC.Controllers
{
    /// <summary>
    /// Abstract class for controllers that need to load collection of articles from the session or file
    /// </summary>
    public abstract class ArticlesLoaderController : Controller
    {
        /// <summary>
        /// Gets a collection of all article from Restful api and stores it in session
        /// </summary>
        /// <returns>IEnumberable collection of articles</returns>
        protected IEnumerable<Article> LoadArticles()
        {
            IEnumerable<Article> arts = null;
            if (Session["articles"] == null)
            {
                var request = WebRequest.Create(WebConfigurationManager.AppSettings["RestfulApiAddress"] + "api/article/");
                request.ContentType = "application/json; charset=utf-8";
                string text;
                var response = (HttpWebResponse)request.GetResponse();

                using (var sr = new StreamReader(response.GetResponseStream()))
                {
                    text = sr.ReadToEnd();
                }
                var serializer = new JavaScriptSerializer();

                arts = serializer.Deserialize<IEnumerable<Article>>(text);
                Session["articles"] = arts;
            }
            else
            {
                arts = (IEnumerable<Article>)Session["articles"];
            }
            return arts;
        }
	}
}