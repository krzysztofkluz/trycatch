﻿using BusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebShopMVC.Controllers
{
    public class ArticlesController : ArticlesLoaderController
    {
        //
        // GET: /Articles/
        /// <summary>
        /// Action generates a view containing list of all articles
        /// </summary>
        /// <returns>View with all articles</returns>
        public ActionResult Index()
        {
            IEnumerable<Article> arts = LoadArticles();
            IShoppingCart cart = (IShoppingCart)Session["cart"];
            ViewBag.CartCount = (cart == null) ? 0 : cart.CountArticles();
            return View(arts);
        }

        /// <summary>
        /// Gets details of single article
        /// </summary>
        /// <param name="id">Id of the article</param>
        /// <returns>Partial view presenting details of the article</returns>
        public PartialViewResult SingleDetails(int id)
        {
            IEnumerable<Article> arts = LoadArticles();
            return PartialView(arts.SingleOrDefault(v => v.Id == id));
        }
	}
}