﻿using BusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebShopMVC.Models;

namespace WebShopMVC.Controllers
{
    public class ShoppingCartController : Controller
    {

        //
        // GET: /ShoppingCart/
        public ActionResult Index()
        {

            IShoppingCart cart = (IShoppingCart)Session["cart"];
            //If cart is not present in the session redirect to main page
            if (cart == null) return RedirectToRoute(new { controller = "Home", action = "Index" });

            ViewBag.CartCount = (cart == null) ? 0 : cart.CountArticles();
            ViewBag.TotalShoppingCartValueExclVat = cart.TotalShoppingCartValueExclVat;
            ViewBag.TotalShoppingCartValueInclVat = cart.TotalShoppingCartValueInclVat;
            ViewBag.TotalShoppingCartVatValue = cart.TotalShoppingCartVatValue;
            return View(new ShoppingCartModel(cart));
        }
        //
        // GET: /ShoppingCart/
        public ActionResult ThankYou()
        {
            //Clears the content of the shopping cart
            Session["cart"] = null;
            ViewBag.CartCount = 0;
            return View();
        }

        public ActionResult Save(string FirstName, string Title, string LastName, string Address, string HouseNumber, string ZipCode, string City, string EmailAddress)
        {
            Customer c = new Customer(Title, FirstName, LastName, Address, HouseNumber, ZipCode, City, EmailAddress);
            IShoppingCart cart = (IShoppingCart)Session["cart"];
            //Saves the order -> customer + shopping cart details
            cart.Save(c);
            return RedirectToAction("ThankYou");
        }
	}
}