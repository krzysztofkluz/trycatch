﻿using BusinessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebShopMVC.Models
{
    public class ShoppingCartModel
    {
        [Display(Name = "First Name")]
        [Required(ErrorMessage = "First name is required")]
        [MaxLength(100,ErrorMessage="First Name too long")]
        public string FirstName
        {
            get;
            set;
        }

        [Display(Name = "Title")]
        [Required(ErrorMessage = "Title is required")]
        [MaxLength(5, ErrorMessage = "Title too long")]
        public string Title
        {
            get;
            set;
        }

        [Display(Name = "Last Name")]
        [Required(ErrorMessage = "Last Name is required")]
        [MaxLength(100, ErrorMessage = "Last Name too long")]
        public string LastName
        {
            get;
            set;
        }

        [Display(Name = "Address")]
        [Required(ErrorMessage = "Address is required")]
        [MaxLength(1024, ErrorMessage = "Address too long")]
        public string Address
        {
            get;
            set;
        }

        [Display(Name = "House Number")]
        [Required(ErrorMessage = "House Number is required")]
        [MaxLength(10, ErrorMessage = "House number too long")]
        public string HouseNumber
        {
            get;
            set;
        }

        [Display(Name = "Zip Code")]
        [Required(ErrorMessage = "Zip Code is required")]
        [MaxLength(10, ErrorMessage = "Zip code too long")]
        public string ZipCode
        {
            get;
            set;
        }

        [Display(Name = "City")]
        [Required(ErrorMessage = "City is required")]
        [MaxLength(100, ErrorMessage = "City too long")]
        public string City
        {
            get;
            set;
        }

        [Display(Name = "Email address")]
        [Required(ErrorMessage = "Email address is required")]
        [EmailAddress(ErrorMessage="Incorrect email format")]
        [MaxLength(512, ErrorMessage = "Email address too long")]
        public string EmailAddress
        {
            get;
            set;
        }
    
        public IShoppingCart Basket{get;set;}

        public ShoppingCartModel(IShoppingCart cart){
            Basket = cart;            
        }

        public void Save()
        {
            Customer c = new Customer(Title, FirstName, LastName, Address, HouseNumber, ZipCode, City, EmailAddress);
            Basket.Save(c);
            
        }

    }
}